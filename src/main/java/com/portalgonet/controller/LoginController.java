package com.portalgonet.controller;

import com.portalgonet.DAO.LoginDAO;
import com.portalgonet.entity.Users;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name= "loginx")
@SessionScoped
public class LoginController {
    private String nombre;
    private String email;
    private String password;
    private String fotoPerfil;

    private Users u;

    public Users getU() {
        return u;
    }

    public void setU(Users u) {
        this.u = u;
    }
    
    public String getFotoPerfil() {
        return fotoPerfil;
    }

    public void setFotoPerfil(String fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }
    
    public String ValidaLogin() throws Exception{
        LoginDAO USER = new LoginDAO();
        u= USER.VlidarUsuario(email,password);
        if(u != null){
            nombre = u.getNameUser();
            fotoPerfil =u.getPicture();
            return "inicio";
        }
        else{
            return"index";
        }
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
