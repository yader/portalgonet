package com.portalgonet.controller;

import com.portalgonet.DAO.ComentariosDAO;
import com.portalgonet.modelo.Comentarios;
import com.portalgonet.modelo.Ideas;
import java.util.List;
import java.util.ListIterator;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "Comentarios")
@SessionScoped
public class ComentariosController {

    private List<Comentarios> listaComentarios;
    private  Comentarios comentario= new Comentarios();

    public Comentarios getComentario() 
    {
        return comentario;
    }

    public void setComentario(Comentarios comentario) 
    {
        this.comentario = comentario;
    }
    
    public List<Comentarios> getListarComentarios() 
    {
        
        return listaComentarios;
    }

    public void setListarComentarios(List<Comentarios> listaComentarios) 
    {
        this.listaComentarios = listaComentarios;
    }
    
    public void registrarComentariosBean(int id,int idU) throws Exception
    {
       ComentariosDAO comentaris;
         
        try
        {
            comentaris=new ComentariosDAO();
            comentaris.registrarComentario(comentario, id,idU);
            this.comentario.setDescripcion("");
        }catch(Exception e)
        {
            throw e;
        }
    }
    
    public void listarComentariosBean(int id) throws Exception
    {
        ComentariosDAO Listacomentarios;
        
        try
        {
            Listacomentarios=new ComentariosDAO();
            listaComentarios=Listacomentarios.consultarComentarios(id);  
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    public void leerIdComentarios(Ideas idea)
    {
        
    }
    
    public void comparaIds(int idIdea,int idIdeaC) throws Exception
    {
        if(idIdea==idIdeaC)
        {
            listarComentariosBean(idIdea);
        }else
        {     
        }
    }
    
    String i="Mostrar Comentarios";
    int j=0;
    
    public void accionBotonComentar()
    {  
        if(i.equals("Ocultar Comentarios"))
        {
            j=0;
        }
        j=1;
    }
    public String msjBotonComentar()
    {
        if(j==1)
        {
            System.out.println("muestra ocultarcomentario");
            return "Ocultar Comentarios";   
        }
        return "Mostrar Comentarios";
    }
    public int totalComentarios(int id) throws Exception
    {
        int total=0;
        ComentariosDAO comentar=new ComentariosDAO();
        total=comentar.totalDeComentarios(id);
        return total;
    }
    public void listarIdUserComentarioBean(){}
    public void MencionarBean(){}
    public void limpiaComentarios()
    {
        this.comentario.setDescripcion("");
    }

}
