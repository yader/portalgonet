package com.portalgonet.controller;

import com.portalgonet.DAO.IdeasDAO;
import com.portalgonet.modelo.Ideas;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="IdeaR")
@SessionScoped
public class IdeasController {
    private Ideas idea= new Ideas();
    private List<Ideas> listarIdeas;
    

    public Ideas getIdea() {
        return idea;
    }

    public void setIdea(Ideas idea) {
        this.idea = idea;
    }

    public List<Ideas> getListarIdeas() {
        return listarIdeas;
    }

    public void setListarIdeas(List<Ideas> listarIdeas) {
        this.listarIdeas = listarIdeas;
    }
    
    public void leerIdIdeasBean(Ideas ideas) throws Exception
    {
        IdeasDAO dao;
        Ideas tmp;
        try
        {
            dao=new IdeasDAO();
            tmp=dao.leerId(ideas);
            if(tmp!=null)
            {
                this.idea=ideas;
            }
            
        }catch(Exception e)
        {
            throw e;
        }
    }
    
    public void listar() throws Exception{
        IdeasDAO dao;
    
        try{
            dao = new IdeasDAO();
            listarIdeas = dao.consultarIdea();
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public void leerId(Ideas leerIdea) throws Exception{
        IdeasDAO dao;
        Ideas temp;
        try{
            dao = new IdeasDAO();
            temp = dao.leerId(leerIdea);
            if(temp != null){
                this.idea =temp;
                
            }
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public String visibleInvisible(String imagen)
   {
       if(imagen.equals(""))
       {
           return "display:none;";
       }
       return "";
   }
    
    public void generavalor()
    {
       this.muestra="display:none;";
       System.out.println("el id de la idea seleccionada es: >>"+this.idea.getId());
    }
    
    public void quitavalor()
    {
        this.muestra="display:inline;";
        System.out.println("quitar valor de id: >>"+this.idea.getId());
    }
    public String mostrarInput()
    {
        if(muestra.equals("display:none;")){
        return "display:inline;";
        }
        return "display:none;";
    }
    public String Updateid()
    {
        int id=this.idea.getId();
        if(id>0)
        {
            return "tabdatos2:n";
        }
        return "tabdatos2:p";
    }
    public String idpanel()
    {
      int id=this.idea.getId();
        if(id>0)
        {
            return "n";
        }
        return "p";  
    }
    
    String muestra="";

    public String getMuestra() {
        return muestra;
    }

    public void setMuestra(String muestra) {
        this.muestra = muestra;
    }
    
    public void lipiaPagIdeas() {
        muestra = "";
        botonMostrar = true;
        botonOculatr = false;
        comentariosMostrar = false;
    }

    public String prueba(String titulo) {
        System.out.println("El titulo es:>>>" + titulo + "<<<<<<<<<");
        return titulo;
    }
    private boolean botonMostrar = true;
    private boolean botonOculatr = false;
    private boolean comentariosMostrar = false;

    public void accionBoton() {
        if (botonMostrar) {
            botonMostrar = false;
            botonOculatr = true;
            setComentariosMostrar(true);
        } else {
            botonMostrar = true;
            botonOculatr = false;
            setComentariosMostrar(false);
        }
    }

    /**
     * @return the botonMostrar
     */
    public boolean isBotonMostrar() {
        return botonMostrar;
    }

    /**
     * @param botonMostrar the botonMostrar to set
     */
    public void setBotonMostrar(boolean botonMostrar) {
        this.botonMostrar = botonMostrar;
    }

    /**
     * @return the botonOculatr
     */
    public boolean isBotonOculatr() {
        return botonOculatr;
    }

    /**
     * @param botonOculatr the botonOculatr to set
     */
    public void setBotonOculatr(boolean botonOculatr) {
        this.botonOculatr = botonOculatr;
    }

    /**
     * @return the comentariosMostrar
     */
    public boolean isComentariosMostrar() {
        return comentariosMostrar;
    }

    /**
     * @param comentariosMostrar the comentariosMostrar to set
     */
    public void setComentariosMostrar(boolean comentariosMostrar) {
        this.comentariosMostrar = comentariosMostrar;
    }
    
}
