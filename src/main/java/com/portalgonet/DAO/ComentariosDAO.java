package com.portalgonet.DAO;

import com.portalgonet.modelo.Comentarios;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ComentariosDAO extends DAO {

    public void registrarComentario(Comentarios comenta,int id,int user) throws  Exception
{
    try
    {
        this.conectarBD();
        PreparedStatement pst=this.getConexion().prepareStatement("insert into comments(ideaId,description,picture,userId,updatedAt) values (?,?,?,?,?)");
        pst.setInt(1,id);
        pst.setString(2,comenta.getDescripcion());
        pst.setString(3,comenta.getImagen());
        pst.setInt(4, user);
        Date fechas= new Date();
        SimpleDateFormat fechFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        String fechaF=fechFormat.format(fechas);
        pst.setString(5,fechaF);
        pst.executeUpdate();
        pst.closeOnCompletion();
    }catch(Exception e)
    {
        throw e;
    }finally
    {
        this.desconectarBD();
    }
}
public List<Comentarios> consultarComentarios(int id) throws Exception
{ 
   List<Comentarios> comentario;
   comentario=new ArrayList();
   ResultSet rs;
   Date fecha;
   SimpleDateFormat fechaformato=new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
   String fechaF;
  
    try
    {
        this.conectarBD();
        PreparedStatement pst=this.getConexion().prepareStatement("select idComments,ideaId,description,picture,createdAt,updatedAt,userId from comments where ideaId="+id);
        rs=pst.executeQuery();
        while(rs.next())
        {
            Comentarios comenta = new Comentarios();
            comenta.setIdComentario(rs.getInt("idComments"));
            comenta.setIdIdea(rs.getInt("ideaId"));
            comenta.setIdUser(rs.getInt("userId"));
            comenta.setDescripcion(rs.getString("description"));
            comenta.setImagen("picture");
            fecha=rs.getTimestamp("createdAt");
            fechaF=fechaformato.format(fecha);
            comenta.setCreacion(fechaF);
            if(rs.getTimestamp("updatedAt").equals(rs.getTimestamp("createdAt")))
            {
                comenta.setActualizacion("sin actualizar");
            }else
            {
                fecha=rs.getTimestamp("updatedAt");
                fechaF=fechaformato.format(fecha); 
                comenta.setActualizacion(fechaF); 
            }
            comentario.add(comenta);
        }
        pst.closeOnCompletion();
        rs.close();
    }catch(Exception e)
    {
        throw e;
    }finally
    {
        this.desconectarBD();
    }
    
    return comentario;
}

public Comentarios leerIdComentario(Comentarios comentario) throws Exception
{
    Comentarios coment=null;
    ResultSet rs;
    try
    {
        this.conectarBD();
        PreparedStatement pst=this.getConexion().prepareStatement("select idComments,ideaId,description,picture from comments where idComments=?");
        pst.setInt(1,comentario.getIdComentario());
        rs=pst.executeQuery();
        while(rs.next())
        {
            coment=new Comentarios();
            coment.setIdComentario(rs.getInt("idComments"));
            coment.setIdIdea(rs.getInt("ideaId"));
            coment.setDescripcion(rs.getString("description"));
            coment.setImagen(rs.getString("picture"));
        }
    }catch(Exception e)
    {
        throw e;
    }finally
    {
        this.desconectarBD();
    }
    return coment;
}
public void eliminarComentario()
{
}
public void megustaComentario()
{
}

public int totalDeComentarios(int id) throws Exception
{
    int total=0;
    ResultSet rs;
    try
    {
        this.conectarBD();
        PreparedStatement pst=this.getConexion().prepareStatement("select count(*) as 'totalComentarios' from comments where ideaId="+id);
        rs=pst.executeQuery();
        rs.next();
        total=rs.getInt("totalComentarios");
    }catch(Exception e)
    {
        throw e;
    }finally
    {
        this.desconectarBD();
    }
    return total;
}

}
