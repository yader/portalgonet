package com.portalgonet.DAO;

import com.portalgonet.entity.Users;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class LoginDAO {
     private EntityManagerFactory emf;
     
     public LoginDAO(){
         emf= Persistence.createEntityManagerFactory("loginPortal");
     }
     
     public Users VlidarUsuario(String email, String password){
        Users user;
        EntityManager em= emf.createEntityManager();
        String sql="SELECT u FROM Users u WHERE u.email = :email AND u.password = :password";
        Query query= em.createQuery(sql);
        query.setParameter("email", email);
        query.setParameter("password", password);
        user=(Users) query.getSingleResult();
       
        return user;
    }
}
