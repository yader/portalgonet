package com.portalgonet.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAO {
    private Connection conexion;

    public Connection getConexion() 
    {
        return conexion;
    }

    public void setConexion(Connection conexion) 
    {
        this.conexion = conexion;
    }
    
    public void conectarBD() throws Exception
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/portalIdeas", "root", "root");
        }catch(ClassNotFoundException e)
        {
            throw e;
        } catch (SQLException e) {
            throw e;
        }
    }
    
    public void desconectarBD() throws Exception
    {
        try
        {
            if(conexion!=null)
            {
                if(conexion.isClosed()==false)
                {
                    conexion.close();
                }
            }
        }catch(Exception e)
        {
            throw e;
        }
        
    }
}
