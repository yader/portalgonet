package com.portalgonet.DAO;

import com.portalgonet.modelo.Ideas;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IdeasDAO extends DAO {

    public List<Ideas> consultarIdea() throws Exception {
        
        List<Ideas> idea;
        ResultSet rs;
        Date fecha;
        SimpleDateFormat fechaformato = new SimpleDateFormat("dd-MM-yyyy HH:mm:s");
        String fechaF;
        
        try {
            this.conectarBD();
            PreparedStatement pst1 = this.getConexion().prepareStatement("select idIdeas,title,description,picture,createdAt,updatedAt from ideas order by idIdeas desc");
            rs = pst1.executeQuery();
            idea = new ArrayList();
            
            while (rs.next()) {
                Ideas ideas = new Ideas();
                ideas.setId(rs.getInt("idIdeas"));
                ideas.setTitulo(rs.getString("title"));
                ideas.setDescripcion(rs.getString("description"));
                ideas.setImagen(rs.getString("picture"));
                fecha = rs.getTimestamp("createdAt");
                fechaF = fechaformato.format(fecha);
                ideas.setCreacion(fechaF);
              
                if (rs.getTimestamp("updatedAt") == (rs.getTimestamp("createdAt"))) {
                    ideas.setActualizacion("sin actualizar");
                } 
                else {
                    fecha = rs.getTimestamp("updatedAt");
                    fechaF = fechaformato.format(fecha);
                    ideas.setActualizacion(fechaF);
                }
                idea.add(ideas);
            }
            
            pst1.closeOnCompletion();
            rs.close();
        } 
        catch (Exception e) {
            throw e;
        } 
        finally {
            this.desconectarBD();
        }
        return idea;
    }

    public Ideas leerId(Ideas idea) throws Exception {
        Ideas ide = null;
        ResultSet rs;
        try {
            this.conectarBD();
            PreparedStatement ps = this.getConexion().prepareStatement("SELECT idIdeas,title,description,picture FROM ideas ORDER BY idIdeas DESC");
            ps.setInt(1, idea.getId());
            rs = ps.executeQuery();
            while (rs.next()) {
                ide = new Ideas();
                ide.setId(rs.getInt("idIdeas"));
                ide.setTitulo(rs.getString("title"));
                ide.setDescripcion(rs.getString("description"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.desconectarBD();
        }
        return ide;
    }
}
